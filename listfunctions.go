package main

import "fmt"

// Erwartet eine Liste und liefert Position und Wert des größten Elements.
func Largest(l []int) (int, int) {
	largestPos := 0
	largestValue := l[0]

	for i, v := range l {
		if v > largestValue {
			largestPos = i
			largestValue = v
		}
	}

	return largestPos, largestValue
}

func Example_Largest() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}
	l3 := []int{1, 2, 3, 4}
	l4 := []int{5, 4, 3, 2}

	fmt.Println(Largest(l1))
	fmt.Println(Largest(l2))
	fmt.Println(Largest(l3))
	fmt.Println(Largest(l4))

	// Output:
	// 6 42
	// 0 2
	// 3 4
	// 0 5
}

// Erwartet eine Liste und liefert die Summe aller Elemente der Liste.
func Sum(l []int) int {
	result := 0
	for _, v := range l {
		result += v
	}
	return result
}

func Example_Sum() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}
	l3 := []int{1, 2, 3, 4}
	l4 := []int{5, 4, 3, 2, 1}

	fmt.Println(Sum(l1))
	fmt.Println(Sum(l2))
	fmt.Println(Sum(l3))
	fmt.Println(Sum(l4))

	// Output:
	// 107
	// 2
	// 10
	// 15
}

// Erwartet eine Liste und liefert den Durchschnitt aller Elemente der Liste.
func Average(l []int) float64 {
	return float64(Sum(l)) / float64(len(l))
}

func Example_Average() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}
	l3 := []int{1, 2, 3, 4}
	l4 := []int{5, 4, 3, 2, 1}

	fmt.Println(Average(l1))
	fmt.Println(Average(l2))
	fmt.Println(Average(l3))
	fmt.Println(Average(l4))

	// Output:
	// 13.375
	// 2
	// 2.5
	// 3
}

// Erwartet eine Liste und liefert den Median der Liste.
func Median(l []int) int {
	// Hinweise:
	// - Der Median ist der mittlere Wert aller Elemente.
	//   Also der Wert, der bei der sortierten Liste genau in der Mitte stehen würde.
	// - Bei gerader Listenlänge soll das Element links von der Mitte verwendet werden.
	return 0
}

func Example_Median() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}
	l3 := []int{1, 2, 3, 4}
	l4 := []int{5, 4, 3, 2, 1}

	fmt.Println(Median(l1))
	fmt.Println(Median(l2))
	fmt.Println(Median(l3))
	fmt.Println(Median(l4))

	// Output:
	// 5
	// 2
	// 2
	// 3
}

// Erwartet eine Liste und eine Zahl n.
// Liefert die Anzahl der Elemente, die größer sind als n.
func CountLarger(l []int, n int) int {
	result := 0

	for _, v := range l {
		if v > n {
			// TODO Hier die Lücke füllen
		}
	}

	return result
}

func Example_CountLarger() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}
	l3 := []int{1, 2, 3, 4}

	fmt.Println(CountLarger(l1, 5))
	fmt.Println(CountLarger(l2, 0))
	fmt.Println(CountLarger(l2, 3))
	fmt.Println(CountLarger(l3, 3))

	// Output:
	// 4
	// 1
	// 0
	// 1
}

// Erwartet eine Liste und eine Zahl n.
// Liefert eine neue Liste, aus der alle Vorkommen von n entfernt worden sind.
func Remove(l []int, n int) []int {
	result := make([]int, 0)

	return result
}

func Example_Remove() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}

	fmt.Println(Remove(l1, 25))
	fmt.Println(Remove(l1, 3))
	fmt.Println(Remove(l1, 6))
	fmt.Println(Remove(l2, 2))

	// Output:
	// [2 1 17 3 12 42 5]
	// [2 1 17 25 12 42 5]
	// [2 1 17 25 3 12 42 5]
	// []
}

// Erwartet zwei Listen l1 und l2.
// Liefert eine neue Liste, die an Stelle i die Summe l1[i] + l2[i] enthält.
func ElementSum(l1, l2 []int) []int {
	result := make([]int, 0)

	return result
}

func Example_ElementSum() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{21, 3, 2, 15, 4, 2, 25, 12}

	fmt.Println(ElementSum(l1, l2))
	fmt.Println(ElementSum(l1, l1))
	fmt.Println(ElementSum(l2, l2))

	// Output:
	// [22 4 19 40 7 14 67 7]
	// [4 2 34 50 6 12 84 10]
	// [42 6 4 30 8 4 50 24]
}
