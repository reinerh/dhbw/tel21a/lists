package main

import "fmt"

// Erwartet einen String und ein Zeichen r.
// Liefert true, falls der String ausschließlich r enthält.
func ContainsOnly(s string, r rune) bool {
	for _, v := range s {
		if v != r {
			return false
		}
	}
	return true
}

func Example_ContainsOnly() {
	fmt.Println(ContainsOnly("aaa", 'a'))
	fmt.Println(ContainsOnly("aaa", 'b'))
	fmt.Println(ContainsOnly("aba", 'b'))
	fmt.Println(ContainsOnly("", 'a'))

	// Output:
	// true
	// false
	// false
	// true
}

// Erwartet einen String und ein Zeichen r.
// Liefert true, falls der String kein Vorkommen von r enthält.
func DoesNotContain(s string, r rune) bool {
	return true
}

func Example_DoesNotContain() {
	fmt.Println(DoesNotContain("aaa", 'a'))
	fmt.Println(DoesNotContain("aaa", 'b'))
	fmt.Println(DoesNotContain("aba", 'b'))
	fmt.Println(DoesNotContain("", 'a'))

	// Output:
	// false
	// true
	// false
	// true
}

// Erwartet einen String und ein Zeichen r.
// Liefert true, falls der String wenigstens ein Vorkommen von r enthält.
func Contains(s string, r rune) bool {
	return true
}

func Example_Contains() {
	fmt.Println(Contains("aaa", 'a'))
	fmt.Println(Contains("aaa", 'b'))
	fmt.Println(Contains("aba", 'b'))
	fmt.Println(Contains("", 'a'))

	// Output:
	// true
	// false
	// true
	// false
}

// Erwartet eine Liste von Strings.
// Liefert true, falls keiner der Strings ein Leerzeichen enthält.
func NoSpaces(l []string) bool {
	for _, line := range l {
		if Contains(line, ' ') {
			return false
		}
	}
	return true
}

func Example_NoSpaces() {
	l1 := []string{"aaa", "bbb"}
	l2 := []string{"aa a", "bb b"}
	l3 := []string{"", "b"}
	l4 := []string{""}
	fmt.Println(NoSpaces(l1))
	fmt.Println(NoSpaces(l2))
	fmt.Println(NoSpaces(l3))
	fmt.Println(NoSpaces(l4))

	// Output:
	// true
	// false
	// true
	// false
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl i.
// Liefert die i-te Zeile der Matrix als String.
func Row(l []string, i int) string {
	return l[i]
}

func Example_Row() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(Row(l1, 0))
	fmt.Println(Row(l1, 1))
	fmt.Println(Row(l1, 2))

	// Output:
	// 123
	// 456
	// 789
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl i.
// Liefert die i-te Spalte der Matrix als String.
func Column(l []string, i int) string {
	result := ""
	return result
}

func Example_Column() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(Column(l1, 0))
	fmt.Println(Column(l1, 1))
	fmt.Println(Column(l1, 2))

	// Output:
	// 147
	// 258
	// 369
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Liefert die Diagonale der Matrix als String, die von links oben nach rechts unten geht.
// Nehmen Sie an, dass die Matrix quadratisch ist.
func DiagDown(l []string) string {
	result := ""
	return result
}

func Example_DiagDown() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(DiagDown(l1))

	// Output:
	// 159
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Liefert die Diagonale der Matrix als String, die von links unten nach rechts oben geht.
// Nehmen Sie an, dass die Matrix quadratisch ist.
func DiagUp(l []string) string {
	result := ""
	return result
}

func Example_DiagUp() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(DiagUp(l1))

	// Output:
	// 358
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Liefert true, falls eine der Zeilen, Spalten oder Diagonalen drei gleiche Zeichen enthält.
func ThreeInARow(l []string) bool {
	return false
}

func Example_ThreeInARow() {
	l1 := []string{"a  ", "a  ", "a  "}
	l2 := []string{"aaa", "   ", "   "}
	l3 := []string{"a  ", " a ", "  a"}
	l4 := []string{"123", "456", "789"}
	fmt.Println(ThreeInARow(l1))
	fmt.Println(ThreeInARow(l2))
	fmt.Println(ThreeInARow(l3))
	fmt.Println(ThreeInARow(l4))

	// Output:
	// true
	// true
	// true
	// false
}

// Erwartet zwei Strings s1 und s2 und liefert true, falls s2 in s1 vorkommt.
func ContainsSubstring(s1, s2 string) bool {
	return false
}

func Example_ContainsSubstring() {
	fmt.Println(ContainsSubstring("Hallo", "H"))
	fmt.Println(ContainsSubstring("Hallo", "h"))
	fmt.Println(ContainsSubstring("Hallo", "Hal"))
	fmt.Println(ContainsSubstring("Hallo", "llo"))
	fmt.Println(ContainsSubstring("Hallo", "ab"))

	// Output:
	// true
	// false
	// true
	// true
	// false
}

// Erwartet eine Liste l von Strings und einen String s.
// Liefert eine neue Liste, die nur noch die Strings aus l enthält,
// in denen s als Teilstring vorkommt.
func FilterForSubstring(l []string, s string) []string {
	result := make([]string, 0)

	return result
}

func Example_FilterForSubstring() {
	l1 := []string{"Hallo", "Welt", "Haus", "Fahrrad"}
	fmt.Println(FilterForSubstring(l1, "Ha"))
	fmt.Println(FilterForSubstring(l1, "rad"))
	fmt.Println(FilterForSubstring(l1, "a"))
	fmt.Println(FilterForSubstring(l1, "lt"))

	// Output:
	// [Hallo Haus]
	// [Fahrrad]
	// [Hallo Haus Fahrrad]
	// [Welt]
}
