package main

func main() {
	Example_Largest()
	Example_Sum()
	Example_Average()
	Example_Median()
	Example_CountLarger()
	Example_Remove()
	Example_ElementSum()

	Example_ContainsOnly()
	Example_DoesNotContain()
	Example_Contains()
	Example_NoSpaces()
	Example_Row()
	Example_Column()
	Example_DiagDown()
	Example_DiagUp()
	Example_ThreeInARow()

	Example_ContainsSubstring()
	Example_FilterForSubstring()
}
